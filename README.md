# The Beatless' PDF fixer

## Overview

This is The Beatless vevkom's collection of scripts to fix sheetmusic PDFs. Currently, only rotation scripts exist, but the framework should be widely extendable.

## Build docker container

```
docker-compose build
```

## Enter docker container

```
docker-compose run app bash
```

## Usage

The entry point is `main.py`, which uses argparse to generate a flexible CLI. The full synopsis for this interface is

```
python main.py [-h] [--clear-output] [--engine ENGINE] [--tessdata-dir TESSDATA_DIR] operation [input] [pages ...]
```

where the second positional argument is `input_type`. `input` is relative path from `input_pdfs` to the file or directory you want to analyze. `input` can be skipped, then the script will take all the files it finds. If `input` is a directory, the script will take all files recursively in that directory. You can also specify which pages you want to analyze. If no pages are provided, all pages will be analyzed. `operation` is the name of the operation you want to perform on each pdf page or image. One of the following:

| Name                      | Description                                                              |
| ------------------------- | ------------------------------------------------------------------------ |
| `rotate_clockwise`        | Rotates a page 90 degrees in a clockwise direction.                      |
| `rotate_counterclockwise` | Rotates a page 90 degrees in a counterclockwise direction.               |
| `rotate_upside_down`      | Rotates a page 180 degrees, aka upside down.                             |
| `crop`                    | Sets the cropbox based on rotation and values given in `OPERATIONS_DICT` |
| `crop_rotate`             | Both crops and rotates with values given in `OPERATIONS_DICT`            |

You can get a more detailed description of the arguments by running the help command

```
python main.py -h
```

There is also a way to clear the output directories:

```
python main.py --clear-output
```
