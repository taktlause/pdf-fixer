import os
import argparse
from io import BytesIO
import mimetypes

from yaml import parse
import PyPDF2
from PyPDF2 import PdfFileReader, PdfFileWriter
from PyPDF2.generic import NameObject, NumberObject
import PIL
import pdf2image
import sheatless
import magic

os.umask(0) # Simplifies management stuff like deleting output files from the code editor on the host system.


def findFilePaths(base_directory, allowed_extensions, case_sensitive_extensions=False):
	"""
	Returns a list of paths to all files with an allowed extension in base_directory

	Example:
	-----------------------
	+-- input_pdfs
	|   +-- foo.pdf
	|   +-- bar.PDF
	|   +-- baz.png
	-----------------------
	findFilePaths("input_pdfs", [".pdf"])
	=> ["input_pdfs/foo.pdf", "input_pdfs/bar.PDF"]
	"""
	if not case_sensitive_extensions: allowed_extensions = [ext.lower() for ext in allowed_extensions]
	res = []
	for (dirpath, dirnames, filenames) in os.walk(base_directory):
		for filename in filenames:
			basename, extension = os.path.splitext(filename)
			if not case_sensitive_extensions: extension = extension.lower()
			if (extension in allowed_extensions):
				res.append(os.path.join(dirpath, filename))
	return res

def clear_dir(directory, recursive=True):
	for (dirpath, dirnames, filenames) in os.walk(directory):
		for filename in filenames:
			os.remove(os.path.join(directory, filename))
		if recursive:
			for dirname in dirnames:
				clear_dir(os.path.join(directory, dirname))
				os.rmdir(os.path.join(directory, dirname))

def tuplify_function_return(ret):
	if ret is None:
		return ()
	if type(ret) is not tuple:
		return (ret,)
	return ret

def image_from_page(page):
	pdf_writer = PyPDF2.PdfFileWriter()
	pdf_writer.addPage(page)
	pdf_stream = BytesIO()
	pdf_writer.write(pdf_stream)
	[img] = pdf2image.convert_from_bytes(pdf_stream.getvalue(), dpi=200)
	return img

def page_from_image(img):
	pdf_stream = BytesIO()
	img.save(pdf_stream, resolution=200, format="PDF")
	pdf_reader = PdfFileReader(pdf_stream, strict=False)
	page = pdf_reader.getPage(0)
	return page

def get_page_rotation(page):
	rotate = page.get("/Rotate", 0)
	if not isinstance(rotate, int):
		rotate = rotate.getObject()
	return rotate

def rotate_page(page, angle):
	assert angle % 90 == 0
	old_angle = get_page_rotation(page)
	page[NameObject("/Rotate")] = NumberObject(old_angle + angle)
	return page

def crop_page(page, x_start=0, y_start=0, x_end=None, y_end=None):
	match get_page_rotation(page):
		case 0:
			page.cropBox.lowerLeft = [x_start, y_start]
			page.cropBox.upperRight = [x_end or page.mediaBox.upperRight[0], y_end or page.mediaBox.upperRight[1]]
		case 90:
			page.cropBox.lowerLeft = [page.mediaBox.upperRight[0] - (y_end or page.mediaBox.upperRight[0]), x_start]
			page.cropBox.upperRight = [page.mediaBox.upperRight[0] - y_start, x_end or page.mediaBox.upperRight[1]]
		case 180:
			page.cropBox.lowerLeft = [page.mediaBox.upperRight[0] - (x_end or page.mediaBox.upperRight[0]), page.mediaBox.upperRight[1] - (y_end or page.mediaBox.upperRight[1])]
			page.cropBox.upperRight = [page.mediaBox.upperRight[0] - x_start, page.mediaBox.upperRight[1] - y_start]
		case -90:
			page.cropBox.lowerLeft = [y_start, page.mediaBox.upperRight[1] - (x_end or page.mediaBox.upperRight[1])]
			page.cropBox.upperRight = [y_end or page.mediaBox.upperRight[0], page.mediaBox.upperRight[1] - x_start]
	return page


OPERATIONS_DICT = {
	"rotate_clockwise": lambda page: rotate_page(page, 90),
	"rotate_counterclockwise": lambda page: rotate_page(page, -90),
	"rotate_upside_down": lambda page: rotate_page(page, 180),
	"crop": lambda page: crop_page(page, 0, 0, None, None),
	"crop_rotate": lambda page: crop_page(rotate_page(page, 180), 150, 0, None, 650),
}


INPUT_PDF_DIR = "input_pdfs"
OUTPUT_PDF_DIR = "output_pdfs"
TMP_PATH = "tmp"

if not os.path.exists(INPUT_PDF_DIR):     os.mkdir(INPUT_PDF_DIR)
if not os.path.exists(OUTPUT_PDF_DIR):    os.mkdir(OUTPUT_PDF_DIR)
if not os.path.exists(TMP_PATH):          os.mkdir(TMP_PATH)

clear_dir(TMP_PATH)


class clear_output_action(argparse.Action):
	def __init__(self, option_strings, dest, **kwargs):
		return super().__init__(option_strings, dest, nargs=0, default=argparse.SUPPRESS, **kwargs)
	
	def __call__(self, parser, namespace, values, option_string, **kwargs):
		clear_dir(OUTPUT_PDF_DIR)

formatter = lambda prog: argparse.ArgumentDefaultsHelpFormatter(prog, max_help_position=50)
parser = argparse.ArgumentParser(description="Develop and test sheatless", formatter_class=formatter)
parser.add_argument("--clear-output", action=clear_output_action, help="Clear output directories first")
parser.add_argument("operation", type=str, choices=OPERATIONS_DICT.keys(), help="Which operation to perform on the selected pages.")
parser.add_argument("input", type=str, nargs="?", default="./", help="Path to input file or directory relative to input_pdfs.")
parser.add_argument("pages", type=str, nargs="*", default="-", help="Select which pages to modify. A space-separated list of page ranges. Example: 1-4 6 8-")
parser.add_argument("--engine", type=str, default="lstm", help="Options: lstm, legacy.")
parser.add_argument("--tessdata-dir", type=str, default="tessdata/tessdata_best-4.1.0/", help="Sets the TESSDATA directory for tesseract.")
args = parser.parse_args()

engine_kwargs = {
	"use_lstm": True if args.engine == "lstm" else False,
	"tessdata_dir": args.tessdata_dir,
}


if os.path.isfile(os.path.join(INPUT_PDF_DIR, args.input)):
	pdf_paths = [os.path.join(INPUT_PDF_DIR, args.input)]
else:
	pdf_paths = findFilePaths(os.path.join(INPUT_PDF_DIR, args.input), [".pdf"])

for pdf_path in pdf_paths:
	# Strip away INPUT_PDF_DIR from pdf_path
	output_path_small = os.path.join(*pdf_path.split("/")[1:])
	# Join with OUTPUT_PDF_DIR to get the output filepath for this pdf
	output_path = os.path.join(OUTPUT_PDF_DIR, output_path_small)
	output_path_dir = os.path.dirname(output_path)
	if not os.path.exists(output_path_dir): os.makedirs(output_path_dir)

	# Here it is sometimes useful to clear the output path
	# clear_dir(output_path)

	pdf_reader = PyPDF2.PdfFileReader(pdf_path, strict=False)
	all_page_nrs = range(1, pdf_reader.getNumPages() + 1)
	operation_page_nrs = set()
	for page_nr_group in args.pages:
		nrs = page_nr_group.split("-")
		start_nr = 1 if nrs[0] == "" else int(nrs[0])
		end_nr = pdf_reader.getNumPages() + 1 if nrs[-1] == "" else int(nrs[-1])
		operation_page_nrs |= set(range(start_nr, end_nr))
	pdf_writer = PyPDF2.PdfFileWriter()
	for page_nr in all_page_nrs:
		try:
			page = pdf_reader.getPage(page_nr - 1)
		except:
			print(f"WARNING: Page number {page_nr} does not exist in {pdf_path}.")
			continue
		if page_nr in operation_page_nrs:
			page = OPERATIONS_DICT[args.operation](page)
		pdf_writer.addPage(page)
	with open(output_path, "wb") as file:
		pdf_writer.write(file)
