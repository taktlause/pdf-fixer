PyPDF2==1.26.0
pdf2image==1.16.0
python-magic==0.4.24
sheatless==1.9.2
Pillow==9.0.0
build
